package main

import (
	"fmt"

	"gitlab.com/user2716871/sum/a/b/c/name"
)

func main() {
	fmt.Println(name.Sum([]int{1, 2, 3, 4}...))
}
